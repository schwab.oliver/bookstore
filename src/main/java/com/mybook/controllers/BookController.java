package com.mybook.controllers;

import com.mybook.entities.Book;
import com.mybook.entities.BookAdminView;
import com.mybook.services.BookService;
import com.mybook.entities.BookCompactView;
import com.mybook.entities.BookDetailView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public List<BookCompactView> getBooks() {
        return this.bookService.getBooks().stream().map(BookCompactView::new).collect(Collectors.toList());
    }

    @GetMapping("/books/{id}")
    public BookDetailView getBook(@PathVariable String id) {
        return new BookDetailView(this.bookService.getBook(Long.parseLong(id)));
    }

    @GetMapping("/books/{id}/admin")
    public BookAdminView getBookForAdmin(@PathVariable String id) {
        return new BookAdminView(this.bookService.getBook(Long.parseLong(id)));
    }

    @PostMapping("/books")
    public ResponseEntity createBook(@RequestBody Book book){
        this.bookService.persistBook(book);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
