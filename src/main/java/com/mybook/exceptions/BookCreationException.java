package com.mybook.exceptions;

public class BookCreationException extends RuntimeException {
    public BookCreationException() {
        super("Book could not be created.");
    }
}
