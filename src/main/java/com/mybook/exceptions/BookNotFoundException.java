package com.mybook.exceptions;

public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(long id) {
        super("Could not find book for id " + id);
    }
}
