package com.mybook.entities;

import com.mybook.enums.Category;

import java.util.List;

public class BookDetailView {
    private Book book;

    public BookDetailView(Book book) {
        this.book = book;
    }

    public long getId() {
        return this.book.getId();
    }

    public String getIsbn() {
        return this.book.getIsbn();
    }

    public String getName() {
        return this.book.getName();
    }

    public String getAuthor() {
        return this.book.getAuthor();
    }

    public List<Category> getCategories() { return this.book.getCategories();}
}
