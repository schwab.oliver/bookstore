package com.mybook.entities;

public class BookCompactView {
    private Book book;

    public BookCompactView(Book book) {
        this.book = book;
    }

    public String getName() {
        return this.book.getName();
    }

    public String getAuthor() {
        return this.book.getAuthor();
    }
}
