package com.mybook.entities;

import java.util.Date;

public class BookAdminView extends BookDetailView {
    private Book book;

    public BookAdminView(Book book) {
        super(book);
        this.book = book;
    }

    public Date getCreatedAt() {
        return this.book.getCreatedAt();
    }

    public Date getModifiedAt() {
        return this.book.getModifiedAt();
    }
}
