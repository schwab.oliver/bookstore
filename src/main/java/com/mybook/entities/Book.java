package com.mybook.entities;

import com.mybook.enums.Category;

import java.util.Arrays;

import java.util.Date;
import java.util.List;

public class Book {
    private long id;
    private String isbn;
    private String name;
    private String author;
    private List<Category> categories;
    private Date createdAt;
    private Date modifiedAt;

    public Book() {
        // default constructor is needed for deserialization
    }

    public Book(long id, String isbn, String name, String author, Category... categories) {
        this.id = id;
        this.isbn = isbn;
        this.name = name;
        this.author = author;
        this.categories = Arrays.asList(categories);
        this.createdAt = new Date();
        this.modifiedAt = new Date();
    }

    public long getId() {
        return id;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }
}
