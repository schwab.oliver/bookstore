package com.mybook.enums;

public enum Category {
    THRILLER,
    DRAMA,
    ROMANCE,
    SCIENCE
}
