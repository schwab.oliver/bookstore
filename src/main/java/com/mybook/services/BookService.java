package com.mybook.services;

import com.mybook.entities.Book;
import com.mybook.exceptions.BookCreationException;
import com.mybook.exceptions.BookNotFoundException;

import java.util.List;

public interface BookService {
    /**
     * Get a list of all books
     */
    List<Book> getBooks();

    /**
     * Get the book for the given id
     */
    Book getBook(long id) throws BookNotFoundException;

    /**
     * Persist the given book
     */
    void persistBook(Book book) throws BookCreationException;
}
