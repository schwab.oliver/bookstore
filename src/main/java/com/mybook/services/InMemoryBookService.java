package com.mybook.services;

import com.mybook.entities.Book;
import com.mybook.enums.Category;
import com.mybook.exceptions.BookCreationException;
import com.mybook.exceptions.BookNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InMemoryBookService implements BookService {
    private List<Book> books;

    public InMemoryBookService() {
        this.books = new ArrayList<>();
        this.books.add(new Book(1, "3499626004", "Eine kurze Geschichte der Zeit", "Stephen Hawking", Category.SCIENCE));
        this.books.add(new Book(2, "3499255065", "Die ewigen Toten", "Simon Beckett", Category.THRILLER));
        this.books.add(new Book(3, "9783499266720", "Ein ganzes halbes Jahr", "Jojo Moyes", Category.DRAMA, Category.ROMANCE));
    }

    @Override
    public List<Book> getBooks() {
        return this.books;
    }

    @Override
    public Book getBook(long id) {
        Book book = this.books.stream().filter(b -> b.getId() == id).findFirst().orElse(null);
        if (book == null) {
            throw new BookNotFoundException(id);
        }

        return book;
    }

    @Override
    public void persistBook(Book book) {
        // check if id and isbn are not already used
        if (this.books.stream().anyMatch(b -> b.getId() == book.getId() || b.getIsbn().equalsIgnoreCase(book.getIsbn()))) {
            throw new BookCreationException();
        }

        this.books.add(book);
    }
}
